import {
  AfterViewInit,
  Component,
  ContentChildren,
  Input,
  QueryList,
  ViewChild,
  Output, EventEmitter
} from '@angular/core';
import {
  MatTab,
  MatTabGroup,
  ThemePalette, MatTabChangeEvent, MatTabHeaderPosition
} from '@angular/material';

import {  } from '@angular/material';

import {CustomTabComponent} from "./custom-tab.component";

@Component({
  selector: 'custom-tabs',
  template: `
    <mat-tab-group #tabGroup
    class="sq-tab-group"
               [backgroundColor]="backgroundColor"
               [color]="color"
               [disableRipple]="disableRipple"
               [dynamicHeight]="dynamicHeight"
               [headerPosition]="headerPosition"
               [selectedIndex]="selectedIndex"
               (selectedIndexChange)="selectedIndexChange($event)"
               (selectedTabChange)="selectedTabChange($event)"
               (focusChange)="focusChange($event)"
    >
      <ng-content #outlet></ng-content>
    </mat-tab-group>
  `
})
export class CustomTabsComponent {
  @ViewChild(MatTabGroup)
  public tabGroup: MatTabGroup;

  @ContentChildren(CustomTabComponent)
  protected tabs: QueryList<CustomTabComponent>;

   /**
     * @description Background color of the tab group.
     */
    @Input()
    backgroundColor?: ThemePalette;
    /**
     * @description Theme color palette for the component.
     */
    @Input()
    color?: ThemePalette;
    /**
     * @description Disables the ripple effects for the whole tab group.
     */
    @Input()
    disableRipple?: boolean;

    /**
     * @description Whether the tab group should grow to the size of the active tab.
     * Applicable when the height of the tab group is fixed.When set to `false` a scrollbar appears.
     */
    @Input()
    dynamicHeight?: boolean;

    /**
     * @description Specifies the header position of the tabs.Accepts two values `above` & `below`.
     */
    @Input()
    headerPosition?: MatTabHeaderPosition;

    /**
     * @description Defines whether the label should appear `before` or `after` the icon.Defaults to after.
     */
    @Input() labelPosition?: 'before' | 'after' = 'after';

    /**
     * @description Sets the active tab.Index starts with zero.
     */
    @Input()
    selectedIndex?: number | null;

    /**
     * @description Event emitted when selectedIndex changes.
     */
    @Output()
    sqSelectedIndexChange?: EventEmitter<number> = new EventEmitter;
    /**
     * @description Event emitted when active tab changes.
     */
    @Output()
    sqSelectedTabChange?: EventEmitter<MatTabChangeEvent> = new EventEmitter;
    /**
     * @description Event emitted when focus is put on any of the tab labels in the header,
     * usually through keyboard navigation.
     */
    @Output()
    sqFocusChange?: EventEmitter<MatTabChangeEvent> = new EventEmitter;

  public ngAfterViewInit() {
    console.log(this.tabs);
    const matTabsFromQueryList = this.tabs.map((tab) => tab.matTab);
    const list = new QueryList<MatTab>();
    list.reset([matTabsFromQueryList]);
    this.tabGroup._tabs = list;
    this.tabGroup.ngAfterContentInit();
  }

   /**
     * @hidden
     * Emits the sqSelectedIndexChange event.
     */
    selectedIndexChange(event: number) {
        this.sqSelectedIndexChange.emit(event);
    }
    /**
     * @hidden
     *  Emits the sqSelectedTabChange event.
     */
    selectedTabChange(event: MatTabChangeEvent) {
        this.sqSelectedTabChange.emit(event);
    }
    /**
     * @hidden
     * Emits the sqFocusChange event.
     */
    focusChange(event: MatTabChangeEvent) {
        this.sqFocusChange.emit(event);
    }
}
