import {
  Component,
  Input,
  ViewChild,
  TemplateRef
} from '@angular/core';
import {
  MatTab,MatTabLabel,
} from '@angular/material';

@Component({
  selector: 'custom-tab',
  template: `
    <mat-tab [label]="label"
             [disabled]="disabled"
    >
    <ng-template *ngIf="tabHeader" mat-tab-label>
    <ng-container *ngTemplateOutlet="tabHeader"></ng-container>
    </ng-template>
      <ng-content></ng-content>
    </mat-tab>
  `
})
export class CustomTabComponent {
  @Input() label?: string;

  @Input() disabled?:boolean;

  @Input() isActive? :boolean;

  @Input() sposition? : number | null;

  @ViewChild(MatTab)
  public matTab: MatTab;

  @Input()
  @ViewChild(MatTabLabel)
  public matTabLabel;

  @Input()
  @ViewChild('tabHeader')
  public tabHeader?: TemplateRef<any>;
}

